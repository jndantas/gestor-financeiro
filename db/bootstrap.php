<?php

use JNDFin\Application;
use JNDFin\Plugins\DbPlugin;
use JNDFin\ServiceContainer;
use JNDFin\Plugins\AuthPlugin;


$serviceContainer = new ServiceContainer();
$app = new Application($serviceContainer);


$app->plugin(new DbPlugin());
$app->plugin(new AuthPlugin());

return $app;