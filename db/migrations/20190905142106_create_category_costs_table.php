<?php

use Phinx\Migration\AbstractMigration;

class CreateCategoryCostsTable extends AbstractMigration
{
    public function up()
    {
        $this->table('category_costs')
            ->addColumn('name', 'string')
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id','users','id')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->save();
    }

    public function down()
    {
        $this->dropTable('category_costs');
    }
}
