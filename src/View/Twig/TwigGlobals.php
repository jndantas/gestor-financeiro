<?php

namespace JNDFin\View\Twig;

use JNDFin\Auth\AuthInterface;

class TwigGlobals extends \Twig\Extension\AbstractExtension implements \Twig\Extension\GlobalsInterface
{
    private $auth;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    public function getGlobals()
    {
        return [
            'Auth' => $this->auth
        ];
    }
}
