<?php
declare(strict_types=1);

namespace JNDFin\Plugins;

use Interop\Container\ContainerInterface;
use JNDFin\ServiceContainerInterface;
use JNDFin\View\Twig\TwigGlobals;
use JNDFin\View\ViewRenderer;

class ViewPlugin implements PluginInterface
{

    public function register(ServiceContainerInterface $container)
    {
        $container->addLazy(
            'twig', function (ContainerInterface $container) {
                $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../templates');
                $twig = new \Twig\Environment($loader);

                $auth = $container->get('auth');
            
                $generator = $container->get('routing.generator');
                $twig->addExtension(new TwigGlobals($auth));
                $twig->addFunction(
                    new \Twig\TwigFunction(
                        'route',
                        function (string $name, array $params = []) use ($generator) {
                            return $generator->generate($name, $params);
                        }
                    )
                );
                return $twig;
            }
        );

        $container->addLazy(
            'view.renderer', function (ContainerInterface $container) {
                $twigEnviroment = $container->get('twig');
                return new ViewRenderer($twigEnviroment);
            }
        );
    }

}
