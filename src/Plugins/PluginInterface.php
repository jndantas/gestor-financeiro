<?php

namespace JNDFin\Plugins;

use JNDFin\ServiceContainerInterface;

interface PluginInterface
{
    public function register(ServiceContainerInterface $container);
}
