<?php
declare(strict_types=1);

namespace JNDFin\Plugins;

use JNDFin\Repository\CategoryCostRepository;
use JNDFin\Repository\RepositoryFactory;
use JNDFin\ServiceContainerInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Interop\Container\ContainerInterface;
use JNDFin\Models\BillPay;
use JNDFin\Models\BillReceive;
use JNDFin\Models\CategoryCost;
use JNDFin\Models\User;
use JNDFin\Repository\StatementRepository;

class DbPlugin implements PluginInterface
{

    public function register(ServiceContainerInterface $container)
    {

        $capsule = new Capsule();
        $config = include __DIR__ . '/../../config/db.php';
        $capsule->addConnection($config['default_connection']);
        $capsule->bootEloquent();

        $container->add('repository.factory', new RepositoryFactory());
        $container->addLazy(
            'category-cost.repository', function () {
                return new CategoryCostRepository();
            }
        );

        $container->addLazy(
            'bill-receive.repository', function (ContainerInterface $container) {
                return $container->get('repository.factory')->factory(BillReceive::class);
            }
        );

        $container->addLazy(
            'bill-pay.repository', function (ContainerInterface $container) {
                return $container->get('repository.factory')->factory(BillPay::class);
            }
        );

        $container->addLazy(
            'users.repository', function (ContainerInterface $container) {
                return $container->get('repository.factory')->factory(User::class);
            }
        );

        $container->addLazy(
            'statement.repository', function () {
                return new StatementRepository();
            }
        );

    }

}
