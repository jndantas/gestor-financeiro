<?php
declare(strict_types=1);

namespace JNDFin\Plugins;

use JNDFin\ServiceContainerInterface;
use Interop\Container\ContainerInterface;
use JNDFin\Auth\Auth;
use JNDFin\Auth\JasnyAuth;

class AuthPlugin implements PluginInterface
{

    public function register(ServiceContainerInterface $container)
    {
        $container->addLazy(
            'jasny.auth', function (ContainerInterface $container) {
                return new JasnyAuth($container->get('users.repository'));
            }
        );
        $container->addLazy(
            'auth', function (ContainerInterface $container) {
                return new Auth($container->get('jasny.auth'));
            }
        );

    }

}
